package com.zuitt.example;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[]args){

        System.out.println("Input an integer whose factorial will be computed:");

        Scanner in = new Scanner(System.in);
        int num = 0;
        try {
            num = in.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter an integer.");
            return;
        }

        if (num < 0) {
            System.out.println("Factorial is not defined for negative numbers.");
            return;
        }

        int answer = 1;
        int counter = 1;

        while (counter <= num) {
            answer *= counter;
            counter++;
        }

        System.out.println(num + "! = " + answer);

    }
}
