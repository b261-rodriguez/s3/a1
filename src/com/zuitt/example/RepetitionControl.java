package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {

    public static void main(String[]args){
        //Loops
            // are control structures that allow code blocks to be executed multiple times
        //While loop

//        int x = 5;
//
//        while(x < 5){
//            System.out.println("Loop Number: " + x);
//            x--;
//        }

        //Do-While loop
            //Similar to while loops. However, do-while loops will execute atleast once - while loop may not execute at all

//        int y = 5;
//        do {
//            System.out.println("Countdown: " + y);
//            y--;
//        } while (y > 5);

        //For loop
        //Syntax:
           /* for(initialValue;condition;iteration){
                //code block
            }*/

        for (int i = 0; i < 10; i++){
            System.out.println("Current count " + i);
        }

        //For Loop with Arrays

        int[] intArray = {100,200,300,400,500};

        for(int i = 0 ; i < intArray.length; i++){
            System.out.println(intArray[i]);
        }

        //For-each loop with Array
            //Syntax:
               /*
                   for(dataType itemName : arrayName){
                        //code block
                    }
                */

 /*       String[] nameArray = {"John", "Paul", "George", "Ringo"};
        for (String name: nameArray){
            System.out.println(name);
        }*/

        //Nested for loops
        String[][] classroom = new String[3][3];

        //First Row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second Row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Aramis";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";


        //outer loop will loop through the rows
        for (int row = 0; row < 3; row++){
            //inner loop will loop through the columns of each row
            for (int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }
        //Access each row;
        for (String[] row: classroom){
            //accessing column (actual element)
            for(String column: row) {
                System.out.println(column);
            }
        }

        //for-each wit Array List
       //Syntax:
            /*
            arrayListName.forEach(consumer -> //code block)

            */
        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);

        System.out.println("Arraylist: " + numbers);

        //"->" this is called lambda operator which is used to separate parameter and implementation
        // -> ~ single arrow
        numbers.forEach(number -> System.out.println("Arraylist: " + number));

        //forEach with HashMaps
        /*

            Syntax:
                hashMapName.forEach((key,value) -> codeBlock);
        */

        HashMap<String, Integer> grades = new HashMap<>(){{
            put("English", 90);
            put("Math", 96);
            put("Science", 97);
            put("HIstrory", 94);
        }};

        grades.forEach((subject, grade)-> System.out.println(subject + ": " + grade));


    }


}
